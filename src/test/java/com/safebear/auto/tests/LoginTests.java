package com.safebear.auto.tests;

import com.safebear.auto.utils.Utils;
import org.testng.Assert;
import org.testng.annotations.Test;

public class LoginTests extends BaseTest {

    @Test
    public void loginTest(){
        //Step 1 open the web app in browser
        driver.get(Utils.getUrl());

        //step 1a assert we're on the login page
        Assert.assertEquals(loginPage.getPageTitle(),"Login Page","The login page didn't open, or the title has changed");

        //step 2 login
        loginPage.login("tester","letmein");

        //step 2a assert we are on the tools page
        Assert.assertEquals(toolsPage.getPageTitle(),"Tools Page","meh");







    }
}

